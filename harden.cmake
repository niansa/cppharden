cmake_minimum_required(VERSION 3.9)

if (NOT MSVC)
option(EXTRA_HARDENING "Enable a bunch of extra hardening compile options" ON)

function(target_hardening TARGET)
    if (NOT EXTRA_HARDENING)
        return()
    endif()

    # LTO
    set_property(TARGET ${TARGET} PROPERTY INTERPROCEDURAL_OPTIMIZATION ON)

    # Warnings/Errors
    set(SAFE_COMPILE_FLAGS -fstrict-enums -fdelete-null-pointer-checks -fstack-protector -Wpedantic -Wall -Wextra -Wconversion -Werror=div-by-zero -Werror=ctor-dtor-privacy -Werror=delete-non-virtual-dtor -Werror=delete-non-virtual-dtor -Werror=reorder -Werror=overloaded-virtual -Werror=mismatched-new-delete -Werror=delete-incomplete -Werror=suggest-override -Werror=format=2 -Werror=nonnull -Werror=null-dereference -Werror=infinite-recursion -Werror=null-dereference -Werror=init-self -Werror=implicit-fallthrough -Werror=missing-braces -Werror=parentheses -Werror=sequence-point -Werror=switch -Werror=switch-bool -Werror=uninitialized -Werror=array-bounds -Werror=bool-operation -Werror=frame-address -Werror=div-by-zero -Werror=float-equal -Werror=pointer-arith -Werror=cast-qual -Werror=cast-align -Werror=cast-function-type -Werror=write-strings -Werror=overflow)
    if (CMAKE_CXX_COMPILER_ID STREQUAL Clang)
        set(SAFE_COMPILE_FLAGS ${SAFE_COMPILE_FLAGS} -Werror=fortify-source -Werror=return-stack-address)
    elseif (CMAKE_CXX_COMPILER_ID STREQUAL GNU)
        set(SAFE_COMPILE_FLAGS ${SAFE_COMPILE_FLAGS} -Werror=return-local-addr)
    endif()
    target_compile_options(${TARGET} PRIVATE ${SAFE_COMPILE_FLAGS})

    # Sanitizers
    if (CMAKE_CXX_COMPILER_ID STREQUAL Clang)
        if (CMAKE_BUILD_TYPE MATCHES "^Rel")
            set(SAFE_COMPILE_FLAGS -fsanitize=undefined,integer,bounds -fsanitize-minimal-runtime)
        else()
            set(SAFE_COMPILE_FLAGS -fsanitize=address,undefined,integer,implicit-integer-truncation,nullability,bool,bounds,enum)
        endif()
        target_compile_options(${TARGET} PUBLIC ${SAFE_COMPILE_FLAGS})
        target_link_options(${TARGET} PUBLIC ${SAFE_COMPILE_FLAGS})
    else()
        message(WARNING "Sanitizers only available with Clang")
    endif()

    # Various protections
    set(SAFE_COMPILE_FLAGS -fno-strict-overflow -fno-strict-aliasing -ftrivial-auto-var-init=pattern)
    if (CMAKE_SYSTEM_PROCESSOR STREQUAL x86_64)
        set(SAFE_COMPILE_FLAGS -fcf-protection=full)
    elseif (CMAKE_SYSTEM_PROCESSOR STREQUAL aarch64)
        set(SAFE_COMPILE_FLAGS -mbranch-protection=standard)
    endif()
    if (CMAKE_CXX_COMPILER_ID STREQUAL GNU)
        set(SAFE_COMPILE_FLAGS ${SAFE_COMPILE_FLAGS} -fanalyzer)
    endif()
    target_compile_options(${TARGET} PRIVATE ${SAFE_COMPILE_FLAGS})
    target_link_options(${TARGET} PRIVATE ${SAFE_COMPILE_FLAGS})

    # More hardening
    set(SAFE_COMPILE_FLAGS -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3)
    target_compile_options(${TARGET} PRIVATE ${SAFE_COMPILE_FLAGS})
endfunction()

else()

function(target_hardening TARGET)
    message(STATUS "Not applying hardening because compiler is MSVC")
endfunction()

endif()
